
import { Component } from 'react';
import logo from '../logo.svg';

class HeaderComponent extends Component{
    render() {
        return (
            <>
                <div className='row form-group'>
                    <div className='col-6 text-center'>
                        <img src={logo} width='150px'></img>
                    </div>
                    <div className='col-6'>
                        <h1>React Store</h1><br/>
                        <h6>Demo App Shop24h v1.0</h6>
                    </div>
                </div>
                <div className='row bg-dark'>
                    <button className='btn btn-dark col-2'> Home</button>
                </div>
            </>
        )
    }
}

export default HeaderComponent;