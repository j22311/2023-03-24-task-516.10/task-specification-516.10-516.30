import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import HeaderComponent from './components/HeaderComponent';
function App() {
  return (
    <div className="container">
        <HeaderComponent/>
    </div>
  );
}

export default App;
